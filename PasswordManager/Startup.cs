﻿using System;
using System.Configuration;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Owin;
using Owin;
using StackExchange.Redis;

[assembly: OwinStartupAttribute(typeof(PasswordManager.Startup))]
namespace PasswordManager
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var redis = ConnectionMultiplexer.Connect(ConfigurationManager.AppSettings["ReditConnection"]);
            services.AddDataProtection()
                .PersistKeysToRedis(redis, "DataProtection-Keys");
        }
    }
}
