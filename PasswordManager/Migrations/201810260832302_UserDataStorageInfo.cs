namespace PasswordManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserDataStorageInfo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserDataStorageInfos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        ProtectorString = c.Binary(),
                        Salt = c.Binary(maxLength: 64),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserDataStorageInfos", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.UserDataStorageInfos", new[] { "UserId" });
            DropTable("dbo.UserDataStorageInfos");
        }
    }
}
