namespace PasswordManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PasswordTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Passwords",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Title = c.String(maxLength: 255),
                        PasswordData = c.Binary(maxLength: 256),
                        Salt = c.Binary(maxLength: 64),
                        Iv = c.Binary(maxLength: 16),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Passwords", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Passwords", new[] { "UserId" });
            DropTable("dbo.Passwords");
        }
    }
}
