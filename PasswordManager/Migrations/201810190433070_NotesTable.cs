namespace PasswordManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Title = c.String(maxLength: 255),
                        NoteData = c.Binary(maxLength: 2560),
                        Salt = c.Binary(maxLength: 64),
                        Iv = c.Binary(maxLength: 16),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notes", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Notes", new[] { "UserId" });
            DropTable("dbo.Notes");
        }
    }
}
