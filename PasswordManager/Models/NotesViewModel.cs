﻿using PasswordManager.Models.Dtos;
using System.Collections.Generic;

namespace PasswordManager.Models
{
    public class NotesViewModel
    {
        public List<NotesDto> Notes { get; set; }
    }
}