﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PasswordManager.Models.Dtos
{
    public class EncryptedDataDto
    {
        public byte[] Data { get; set; }
        public byte[] Iv { get; set; }
        public byte[] Salt { get; set; }
    }
}