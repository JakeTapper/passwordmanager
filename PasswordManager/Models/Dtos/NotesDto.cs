﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PasswordManager.Models.Dtos
{
    public class NotesDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
    }
}