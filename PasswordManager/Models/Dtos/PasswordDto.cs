﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PasswordManager.Models.Dtos
{
    public class PasswordDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Password { get; set; }
    }
}