﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Common;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using PasswordManager.Models.DatabaseModels;

namespace PasswordManager.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public DbSet<Password> Passwords { get; set; }
        public DbSet<Note> Notes { get;set; }
        public DbSet<File> Files { get; set; }
        public DbSet<UserDataStorageInfo> UserDataStorageInfos { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}