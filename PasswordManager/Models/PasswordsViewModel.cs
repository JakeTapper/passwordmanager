﻿using System.Collections.Generic;
using PasswordManager.Models.Dtos;

namespace PasswordManager.Models
{
    public class PasswordsViewModel
    {
        public List<PasswordDto> Passwords { get; set; }
    }
}