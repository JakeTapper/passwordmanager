﻿using System.Collections.Generic;
using PasswordManager.Models.Dtos;

namespace PasswordManager.Models
{
    public class FilesViewModel
    {
        public List<FilesDto> Files { get; set; }
    }
}