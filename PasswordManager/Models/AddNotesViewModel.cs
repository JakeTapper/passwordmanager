﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PasswordManager.Models
{
    public class AddNotesViewModel
    {
        [MaxLength(255)]
        [Required]
        public string Title { get; set; }

        [Required]
        [MaxLength(2560)]
        public string Note { get; set; }
    }
}