﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace PasswordManager.Models.DatabaseModels
{
    [Table("UserDataStorageInfos")]
    public class UserDataStorageInfo
    {
        public int Id { get; set; }
        public string UserId { get; set; }

        public byte[] ProtectorString { get; set; }
        [MaxLength(64)]
        public byte[] Salt { get;set; }

        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
    }
}