﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PasswordManager.Models.DatabaseModels
{
    [Table("Passwords")]
    public class Password
    {
        public int Id { get; set; }
        public string UserId { get; set; }

        [MaxLength(255)]
        public string Title { get; set; }

        [MaxLength(256)]
        public byte[] PasswordData { get; set; }
        [MaxLength(64)]
        public byte[] Salt { get;set; }
        [MaxLength(16)]
        public byte[] Iv { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
    }
}