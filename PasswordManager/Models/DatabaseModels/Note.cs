﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PasswordManager.Models.DatabaseModels
{
    [Table("Notes")]
    public class Note
    {
        public int Id { get; set; }
        public string UserId { get; set; }

        [MaxLength(255)]
        public string Title { get; set; }

        [MaxLength(2560)]
        public byte[] NoteData { get; set; }
        [MaxLength(64)]
        public byte[] Salt { get;set; }
        [MaxLength(16)]
        public byte[] Iv { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
    }
}