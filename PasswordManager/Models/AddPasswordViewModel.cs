﻿using System.ComponentModel.DataAnnotations;

namespace PasswordManager.Models
{
    public class AddPasswordViewModel
    {
        [MaxLength(255)]
        [Required]
        public string Title { get; set; }

        [Required]
        [MaxLength(255)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Passwords do not match")]
        public string ConfirmPassword { get; set; }
    }
}