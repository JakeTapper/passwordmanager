﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceProcess;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.DataProtection;
using PasswordManager.Models;
using PasswordManager.Models.DatabaseModels;
using PasswordManager.Models.Dtos;
using StackExchange.Redis;

namespace PasswordManager.Controllers
{
    [Authorize]
    public class PasswordsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly DataProtector _protector;

        public PasswordsController()
        {
            _context = new ApplicationDbContext();
            _protector = new DpapiDataProtector("PasswordManager", "User-Key-Storage");
        }

        public ActionResult Index()
        {
            var passwords = _context.Passwords.AsNoTracking().Where(x => x.User.UserName == User.Identity.Name).ToList().Select(x => 
                new PasswordDto {
                    Id = x.Id,
                    Title = x.Title,
                    Password = Decrypt(x.PasswordData, x.Iv, x.Salt)
            }).ToList();

            var viewmodel = new PasswordsViewModel
            {
                Passwords = passwords
            };

            return View(viewmodel);
        }

        public ActionResult SecureNotes()
        {
            var notes = _context.Notes.AsNoTracking().Where(x => x.User.UserName == User.Identity.Name).ToList().Select(
                x =>
                    new NotesDto
                    {
                        Id = x.Id,
                        Title = x.Title,
                        Note = Decrypt(x.NoteData, x.Iv, x.Salt)
                    }).ToList();

            var viewmodel = new NotesViewModel
            {
                Notes = notes
            };

            return View(viewmodel);
        }

        public ActionResult Files()
        {
            var files = _context.Files.AsNoTracking().Where(x => x.User.UserName == User.Identity.Name).ToList().Select(
                x =>
                    new FilesDto
                    {
                        Id = x.Id,
                        Title = x.Title
                    }).ToList();

            var viewmodel = new FilesViewModel()
            {
                Files = files
            };

            return View(viewmodel);
        }

        public FileResult DownloadFile(int id)
        {
            var file = _context.Files.FirstOrDefault(x => x.Id == id);
            if (file == null)
            {
                return null;
            }

            if (file.UserId != User.Identity.GetUserId())
            {
                return null;
            }

            var bytes = DecryptToBytes(file.FileData, file.Iv, file.Salt);
            return File(bytes, System.Net.Mime.MediaTypeNames.Application.Octet, file.Title);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddPassword(string title, string password, string confirmPassword)
        {
            title = HttpUtility.UrlDecode(title);
            password = HttpUtility.UrlDecode(password);
            confirmPassword = HttpUtility.UrlDecode(confirmPassword);

            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(confirmPassword))
            {
                return new JsonResult {Data = new {success = false, message = "All fields are required" }};
            }

            if (password != confirmPassword)
            {
                return new JsonResult {Data = new {success = false, message = "Passwords do not match" }};
            }

            if (title.Length > 255)
            {
                return new JsonResult {Data = new {success = false, message = $"Title has a 255 character limit ({title.Length})" }};
            }

            if (password.Length > 255)
            {
                return new JsonResult {Data = new {success = false, message = $"Password has a 255 character limit ({password.Length}). Please use a secure note instead (2500 character limit)." }};
            }
            
            var passwordCryptoData = Encrypt(password);

            if (passwordCryptoData != null)
                _context.Passwords.Add(new Password
                {
                    Title = title,
                    UserId = User.Identity.GetUserId(),
                    PasswordData = passwordCryptoData.Data,
                    Salt = passwordCryptoData.Salt,
                    Iv = passwordCryptoData.Iv
                });
            
            return _context.SaveChanges() > 0
                ? new JsonResult {Data = new {success = true, message = "Password Saved"}}
                : new JsonResult {Data = new {success = false, message = "Error saving password"}};
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddNote(string title, string note)
        {
            title = HttpUtility.UrlDecode(title);
            note = HttpUtility.UrlDecode(note);

            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(note))
            {
                return new JsonResult {Data = new {success = false, message = "All fields are required" }};
            }

            if (title.Length > 255)
            {
                return new JsonResult {Data = new {success = false, message = $"Title has a 255 character limit ({title.Length})" }};
            }

            if (note.Length > 2500)
            {
                return new JsonResult {Data = new {success = false, message = $"Note has a 2500 character limit ({note.Length})" }};
            }

            var noteCryptoData = Encrypt(note);

            if (noteCryptoData != null)
                _context.Notes.Add(new Note
                {
                    Title = title,
                    UserId = User.Identity.GetUserId(),
                    NoteData = noteCryptoData.Data,
                    Salt = noteCryptoData.Salt,
                    Iv = noteCryptoData.Iv
                });
            
            return _context.SaveChanges() > 0
                ? new JsonResult {Data = new {success = true, message = "Note Saved"}}
                : new JsonResult {Data = new {success = false, message = "Error saving note"}};
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddFile(HttpPostedFileBase file, string title)
        {
            title = HttpUtility.UrlDecode(title);

            if (string.IsNullOrEmpty(title) || file == null)
            {
                return new JsonResult {Data = new {success = false, message = "All fields are required" }};
            }

            if (title.Length > 255)
            {
                return new JsonResult {Data = new {success = false, message = $"Title has a 255 character limit ({title.Length})" }};
            }

            MemoryStream target = new MemoryStream();
            file.InputStream.CopyTo(target);
            var data = target.ToArray();

            if (data.Length >= 33554432)
            {
                return new JsonResult {Data = new {success = false, message = "Files cannot be larger than 32MB." }};
            }

            var passwordCryptoData = Encrypt(byt: data);

            if (passwordCryptoData != null)
                _context.Files.Add(new PasswordManager.Models.DatabaseModels.File
                {
                    Title = title,
                    UserId = User.Identity.GetUserId(),
                    FileData = passwordCryptoData.Data,
                    Salt = passwordCryptoData.Salt,
                    Iv = passwordCryptoData.Iv
                });
            
            return _context.SaveChanges() > 0
                ? new JsonResult {Data = new {success = true, message = "File Saved"}}
                : new JsonResult {Data = new {success = false, message = "Error saving file"}};
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditPassword(int id, string title, string password, string confirmPassword)
        {
            title = HttpUtility.UrlDecode(title);
            password = HttpUtility.UrlDecode(password);
            confirmPassword = HttpUtility.UrlDecode(confirmPassword);

            var currentPassword = _context.Passwords.FirstOrDefault(x => x.Id == id);
            if (currentPassword == null)
            {
                return new JsonResult { Data = new {success = false, message = "Password does not exist; Try refreshing the page."}};
            }

            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(confirmPassword))
            {
                return new JsonResult {Data = new {success = false, message = "All fields are required" }};
            }

            if (password != confirmPassword)
            {
                return new JsonResult {Data = new {success = false, message = "Passwords do not match" }};
            }

            if (title.Length > 255)
            {
                return new JsonResult {Data = new {success = false, message = $"Title has a 255 character limit ({title.Length})" }};
            }

            if (password.Length > 256)
            {
                return new JsonResult {Data = new {success = false, message = $"Password has a 256 character limit ({password.Length}). Please use a secure note instead (2500 character limit)." }};
            }

            var passwordCryptoData = Encrypt(password);

            if (passwordCryptoData != null)
            {
                currentPassword.Title = title;
                currentPassword.PasswordData = passwordCryptoData.Data;
                currentPassword.Salt = passwordCryptoData.Salt;
                currentPassword.Iv = passwordCryptoData.Iv;
            }
            return _context.SaveChanges() > 0
                ? new JsonResult {Data = new {success = true, message = "Password Saved"}}
                : new JsonResult {Data = new {success = false, message = "Error editing password"}};

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditNote(int id, string title, string note)
        {
            title = HttpUtility.UrlDecode(title);
            note = HttpUtility.UrlDecode(note);

            var currentNote = _context.Notes.FirstOrDefault(x => x.Id == id);
            if (currentNote == null)
            {
                return new JsonResult { Data = new {success = false, message = "Note does not exist; Try refreshing the page."}};
            }

            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(note))
            {
                return new JsonResult {Data = new {success = false, message = "All fields are required" }};
            }

            if (title.Length > 255)
            {
                return new JsonResult {Data = new {success = false, message = $"Title has a 255 character limit ({title.Length})" }};
            }

            if (note.Length > 2500)
            {
                return new JsonResult {Data = new {success = false, message = $"Note has a 2500 character limit ({note.Length})" }};
            }

            var noteCryptoData = Encrypt(note);

            if (noteCryptoData != null)
            {
                currentNote.Title = title;
                currentNote.NoteData = noteCryptoData.Data;
                currentNote.Salt = noteCryptoData.Salt;
                currentNote.Iv = noteCryptoData.Iv;
            }
            return _context.SaveChanges() > 0
                ? new JsonResult {Data = new {success = true, message = "Note Saved"}}
                : new JsonResult {Data = new {success = false, message = "Error editing note"}};

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePassword(int id)
        {
            var password = _context.Passwords.FirstOrDefault(x => x.Id == id);
            if (password == null)
            {
                return new JsonResult { Data = new {success = false, message = "Password does not exist; Try refreshing the page."}};
            }

            if (password.UserId != User.Identity.GetUserId())
            {
                return new JsonResult { Data = new {success = false, message = "Cannot delete another user's password."}};
            }

            _context.Passwords.Remove(password);
            return _context.SaveChanges() > 0
                ? new JsonResult {Data = new {success = true, message = "Password Deleted"}}
                : new JsonResult {Data = new {success = false, message = "Error deleting password"}};
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteNote(int id)
        {
            var note = _context.Notes.FirstOrDefault(x => x.Id == id);
            if (note == null)
            {
                return new JsonResult { Data = new {success = false, message = "Note does not exist; Try refreshing the page."}};
            }

            if (note.UserId != User.Identity.GetUserId())
            {
                return new JsonResult { Data = new {success = false, message = "Cannot delete another user's note."}};
            }

            _context.Notes.Remove(note);
            return _context.SaveChanges() > 0
                ? new JsonResult {Data = new {success = true, message = "Note Deleted"}}
                : new JsonResult {Data = new {success = false, message = "Error deleting note"}};
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteFile(int id)
        {
            var file = _context.Files.FirstOrDefault(x => x.Id == id);
            if (file == null)
            {
                return new JsonResult { Data = new {success = false, message = "Note does not exist; Try refreshing the page."}};
            }

            if (file.UserId != User.Identity.GetUserId())
            {
                return new JsonResult { Data = new {success = false, message = "Cannot delete another user's note."}};
            }

            _context.Files.Remove(file);
            return _context.SaveChanges() > 0
                ? new JsonResult {Data = new {success = true, message = "File Deleted"}}
                : new JsonResult {Data = new {success = false, message = "Error deleting file"}};
        }

        #region HelperFunctions
        private EncryptedDataDto Encrypt(string str = null, byte[] byt = null)
        {
            var dto = new EncryptedDataDto();
            try
            {
                var saltLength = new Random(DateTime.Now.Millisecond).Next(32, 64);
                var salt = new byte[saltLength];

                var random = new RNGCryptoServiceProvider();
                random.GetBytes(salt);

                var iv = new byte[16];
                random.GetBytes(iv);

                //var currentLoginPassword = GetUserKey();
                //var aes = new AesCryptoServiceProvider();
                //var pwb = new Rfc2898DeriveBytes(currentLoginPassword, salt, 2);

                var aes = new AesCryptoServiceProvider();
                var encryptor = aes.CreateEncryptor(GetUserKey(), iv);

                dto.Salt = salt;
                dto.Iv = iv;

                using (var memoryStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        if (byt == null)
                        {
                            using (var streamWriter = new StreamWriter(cryptoStream))
                            {
                                streamWriter.Write(str);
                            }
                        }
                        else
                        {
                            using (var binaryWriter = new BinaryWriter(cryptoStream))
                            {
                                binaryWriter.Write(byt);
                            }
                        }
                        var passwordData = memoryStream.ToArray();

                        dto.Data = passwordData;
                    }
                }

                return dto;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        private string Decrypt(byte[] data, byte[] iv, byte[] salt)
        {
            try
            {
                //var currentLoginPassword = GetUserKey();
                //var aes = new AesCryptoServiceProvider();
                //var pwb = new Rfc2898DeriveBytes(currentLoginPassword, salt, 2);

                var aes = new AesCryptoServiceProvider();
                var decryptor = aes.CreateDecryptor(GetUserKey(), iv);

                using (var memoryStream = new MemoryStream(data))
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (var streamReader = new StreamReader(cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
            catch
            {
                return null;
            }
        }
        private byte[] DecryptToBytes(byte[] data, byte[] iv, byte[] salt)
        {
            try
            {
                //var currentLoginPassword = GetUserKey();
                //var aes = new AesCryptoServiceProvider();
                //var pwb = new Rfc2898DeriveBytes(currentLoginPassword, salt, 2);

                var aes = new AesCryptoServiceProvider();
                var decryptor = aes.CreateDecryptor(GetUserKey(), iv);

                using (var memoryStream = new MemoryStream(data))
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        var plain = new byte[data.Length];
                        var count = cryptoStream.Read(plain, 0, plain.Length);
                        var returnval = new byte[count];
                        Array.Copy(plain, returnval, count);
                        return returnval;
                    }
                }
            }
            catch
            {
                return null;
                
            }
        }

        private byte[] GetUserKey()
        {
            var protectorString = _context.UserDataStorageInfos
                .FirstOrDefault(x => x.User.UserName == User.Identity.Name)?.ProtectorString;

            return protectorString == null 
                ? null 
                : _protector.Unprotect(protectorString);

            //return (string)System.Web.HttpContext.Current.Session["UserPassword"];
        }
        #endregion
    }
}